'use-strict';

import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import babel from 'gulp-babel';
import browserSync from 'browser-sync';

const reload = browserSync.reload;

gulp.task('serve', ['sass', 'vendors', 'scripts'], () => {
  browserSync({
    proxy: '<LOCAL URL>'
  });
  gulp.watch('templates/**/**/**/*.twig').on('change', reload);
  gulp.watch('js/*.js', ['scripts']).on('change', reload);
  gulp.watch('sass/**/*.scss', ['sass']);
});

gulp.task('sass', () => {
  gulp.src('sass/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'compressed',
    includePaths: './node_modules/'
  }).on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: 'last 2 versions'
  }))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('web/stylesheets'))
  .pipe(browserSync.stream());
});

gulp.task('vendors', () => {
  gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/popper.js/dist/umd/popper.js',
    'node_modules/bootstrap/dist/js/bootstrap.js'
  ])
  .pipe(concat('vendors.js'))
  .pipe(uglify())
  .pipe(gulp.dest('web/js'));
});

gulp.task('scripts', () => {
  gulp.src('js/*.js')
  .pipe(babel())
  .pipe(uglify())
  .pipe(gulp.dest('web/js'));
});

gulp.task('default', ['serve']);